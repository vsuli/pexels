$(document).ready(function () {
    menuBar();
    overlay();
    chooseLanguage();
    formSearch();
    keyDown();

    //slide cat
    $('.content .cat').not('.slick-initialized').slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        centerMode: false,
        variableWidth: true,
        prevArrow: '<button class="slide-arrow prev-arrow"><i class="las la-angle-left"></i></button>',
        nextArrow: '<button class="slide-arrow next-arrow"><i class="las la-angle-right"></i></button>'
    });

    $('.content .list-tab').not('.slick-initialized').slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        centerMode: false,
        variableWidth: true,
        prevArrow: '<button class="slide-arrow prev-arrow"><i class="las la-angle-left"></i></button>',
        nextArrow: '<button class="slide-arrow next-arrow"><i class="las la-angle-right"></i></button>'
    });


    // click filter
    $('.content--top__main--filter button').click(function () {
        $('.form--filter').toggle();
    });
    //select
    $(".form--filter button").click(function () {
        $(this).parent().find('.dropdown').toggle();
        $(this).find('.open,.close').toggle();
    });

    // option
    $('.dropdown li').click(function () {
        $value = $(this).attr('data-value');
        $all = $(this).attr('data-all')
        $(this).parent().parent().find('.value span').html($value);
        $('.js-selected').hide();
        $(this).append('<i class="js-selected las la-check"></i>');
        $(this).parent().hide();
        $(this).parent().parent().addClass("select");
        if ($all !== '1') {
            $(this).parent('.dropdown').parent('.form--filter__item').css({
                'background': '#ededed'
            })
        } else {
            $(this).parent('.dropdown').parent('.form--filter__item').css('background', 'rgba(0,0,0,0)')
        }
    });

    //close popup
    $(".popup--detail").click(function (e) {
        // get Masonry instance
        var overlay = $(".popup--detail__content");
        if (!overlay.is(e.target) && overlay.has(e.target).length === 0) {
            $(".popup--detail").hide();
            $('html,body').css('overflow-y', 'auto');
        }
    });

    $(".popup--detail__close").click(function () {
        $(".popup--detail").hide()
    });


    // close popup collect
    $(".popup--collect__close").click(function () {
        $(".popup--collect").hide();
    });

    //open popup collect
    $(".collect").click(function () {
        $(".popup--collect").show();
        $(".popup--detail").hide();
        //set height
        $(window).resize(function () {
            setHeight();
        })
        //set height popup collect
        setHeight();
    });

    $(".popup--collect").click(function (e) {
        var overlayCollect = $(".popup--collect__content");
        if (!overlayCollect.is(e.target) && overlayCollect.has(e.target).length === 0) {
            $(".popup--collect").hide()
        }
    });

    loadImage();

    openSlideDetail();

    loadImage2();

});


function chooseLanguage() {
    $('.header--menu .js-flag').click(function () {
        $('.overlay, .popup--flag').show();
    });
}

function overlay() {
    $('.overlay').click(function () {
        $('.popup--flag, .overlay').hide();
    });
}

function setHeight() {
    var album = $(".popup--collect__content .album");
    $(".popup--collect__content .album .album--image").height(album.width())
}

function loadImage() {
    //loader
    let $grid = $('.main .grid').masonry({
        itemSelector: 'none', // select none at first
        columnWidth: '.grid__col-sizer',
        gutter: '.grid__gutter-sizer',
        percentPosition: true,
        stagger: 30,
        transitionDuration: '0.3s',
        visibleStyle: {transform: 'translateY(0)', opacity: 1},
        hiddenStyle: {transform: 'translateY(100px)', opacity: 0},
    });

    // get Masonry instance
    let msnry = $grid.data('masonry');

    // initial items reveal
    $grid.imagesLoaded(function () {
        $grid.removeClass('are-images-unloaded');
        $grid.masonry('option', {itemSelector: '.grid__item'});
        let $items = $grid.find('.grid__item');
        $grid.masonry('appended', $items);
    });
    // init Infinte Scroll

    $grid.infiniteScroll({
        path: getPenPath,
        append: '.grid__item',
        outlayer: msnry,
        status: '.page-load-status',
        history: true,
    });

    $grid.on('append.infiniteScroll', function () {
        openSlideDetail();
    });

    function getPenPath() {
        return `list.html`;
    };
}

function openSlideDetail() {
    //click image show popup
    $(".grid__item").click(function () {
        $('.popup--detail').show();
        $('html,body').css('overflow-y', 'clip');

        //slick popup
        $('.popup--detail .slide-detail').not('.slick-initialized').slick({
            fade: true,
            speed: 100,
            prevArrow: '<button class="slide-arrow prev-arrow"><i class="las la-angle-left"></i></button>',
            nextArrow: '<button class="slide-arrow next-arrow"><i class="las la-angle-right"></i></button>'
        });
    });


}

function formSearch() {
    $(document).click(function (e) {
        var search = $(".header--search");
        if (!search.is(e.target) && search.has(e.target).length === 0) {
            $("#form-search .header--search__box").hide();
        }
    });


    // click form
    $(".header--search").click(function () {
        $("#form-search .header--search__box").show();
    });
}

function keyDown() {
    //keydown broad
    $(document).on('keydown', function (evt) {
        if (evt.keyCode == 39) { //39:next
            $('.popup--detail .slide-detail').slick("slickNext");
            console.log('next')
        } else if (evt.keyCode == 37) { // 37 prev
            $('.popup--detail .slide-detail').slick("slickPrev");
        }
    });
}

function loadImage2() {
//-------------------------------------//
// init Masonry
    $grid2 = $('.popup--detail__content .b-more .grid2').masonry({
        itemSelector: 'none', // select none at first
        columnWidth: '.grid__col-sizer2',
        gutter: '.grid__gutter-sizer2',
        percentPosition: false,
        stagger: 30,
        // nicer reveal transition
        visibleStyle: {transform: 'translateY(0)', opacity: 1},
        hiddenStyle: {transform: 'translateY(100px)', opacity: 0},
    });

// get Masonry instance
    let msnry2 = $grid2.data('masonry');

// initial items reveal
    $grid2.imagesLoaded(function () {
        $grid2.removeClass('are-images-unloaded2');
        $grid2.masonry('option', {itemSelector: '.grid__item'});
        let $items2 = $grid2.find('.grid__item');
        $grid2.masonry('appended', $items2);
        console.log($items2)
    });

// init Infinte Scroll
    $grid2.infiniteScroll({
        path: getPenPath2,
        append: '.grid__item',
        outlayer: msnry2,
        status: '.page-load-status2',
        history: true,
    });

// hack CodePen to load pens as pages

    function getPenPath2() {
        return `index.html`;
    }
}


function menuBar() {
    //open menu mobile
    $(".bars-mb").click(function () {
        $('header').toggleClass('check-menu-mb');
        $(".header--menu.mb .submenu").toggleClass('open-menu-mb');
        $(".header--menu.mb .la-bars, .header--menu.mb .la-times").toggleClass('close');
    });
}